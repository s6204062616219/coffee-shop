import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Order } from './order.model';
@Component({
  selector: 'app-coffeeshop',
  templateUrl: './coffeeshop.component.html',
  styleUrls: ['./coffeeshop.component.css']
})

export class CoffeeshopComponent implements OnInit {
  form !: FormGroup;
  order: Order;
  orders: Array<Order>=[];
  constructor(private fb: FormBuilder) {
    this.order = new Order("","",0,"","",false);
  }


   ngOnInit(): void {
     this.form = this.fb.group({
      Name:['',Validators.required,],
      Email: ['',[Validators.required,Validators.email]],
      Phone: ['',[Validators.required,Validators.minLength(10)]],
      Drink: '',
      TempPreference:'',
      SendText: ''
     })
   }

   onSubmit(f:FormGroup):void{
     this.order.name = f.get('Name')?.value;
     this.order.email = f.get('Email')?.value;
     this.order.phone = f.get('Phone')?.value;
     this.order.drink = f.get('Drink')?.value;
     this.order.tempPreference = f.get('TempPreference')?.value;
     this.order.sendText = f.get('SendText')?.value;

     let form_record = new Order(f.get('Name')?.value,
                                  f.get('Email')?.value,
                                  f.get('Phone')?.value,
                                  f.get('Drink')?.value,
                                  f.get('TempPreference')?.value,
                                  f.get('SendText')?.value);
      this.orders.push(form_record);
   }

}

