import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Friend } from './friend';



@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {
friend: Friend; //test1
friends: Array<Friend>=[]  //test2
friendForm !: FormGroup;
  constructor(private fb: FormBuilder) {
    this.friend = new Friend("bob","bob@jmail.com",42)
   }

  ngOnInit(): void {
    this.friendForm = this.fb.group({
      FriendName: [''],
      FriendEmail: [''],
      FriendAge: ['']
    })
  }

  changeDefaultName(str:string){
    this.friend.name = str;
  }

  onSubmit(f:FormGroup):void{
    this.friend.name = f.get('FriendName')?.value;
    this.friend.email = f.get('FriendEmail')?.value;
    this.friend.age = f.get('FriendAge')?.value;
    let form_record = new Friend(f.get('FriendName')?.value,
                                  f.get('FriendEmail')?.value,
                                  f.get('FriendAge')?.value);
    this.friends.push(form_record);
  }
}
