import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MyFormComponent } from './components/my-form/my-form.component';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { DirectivetestComponent } from './components/directivetest/directivetest.component';
import { AppRoutingModule } from './app-routing.module';
import { CoffeeshopComponent } from './components/coffeeshop/coffeeshop.component';
import { GreetComponent } from './components/greet/greet.component';
import { FriendsComponent } from './components/friends/friends.component';
import { ContactComponent } from './components/contact/contact.component';


@NgModule({
  declarations: [
    AppComponent,
    MyFormComponent,
    DirectivetestComponent,
    CoffeeshopComponent,
    GreetComponent,
    FriendsComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
